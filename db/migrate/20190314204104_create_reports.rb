class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.references :hack, foreign_key: true
      t.text :reason
      t.string :ipaddress
      t.boolean :read

      t.timestamps
    end
  end
end
