class CreateDataRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :data_requests do |t|
      t.integer :record_type
      t.integer :record_id
      t.references :user, foreign_key: true
      t.text :reason

      t.timestamps
    end
  end
end
