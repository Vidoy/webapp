# How To Contribute
First thank you for taking the time to contribute to HacksDB, we really appreciate all of the community support.

## Search for an Issue
The easiest way for any community member to contribute (with or without software engineering knowledge) is to find an issue, and create a GitLab Issue if it's not already being tracked.

## What types of merge requests are we unable to accept
A lot of work goes into reviewing each merge request. At least two members of our team will review a proposed change before it's accepted. We do not want to see the overall code quality drop so we take our time in the review and quality assurance process. Because of this we're unable to accept the following types of merge requests and will have close merge requests that fall into these categories:

  * Merge requests that do not reference a GitLab Issue. GitLab Issues serve as a discussion board for our team and community stakeholders to discuss a proposed feature, bug, or other changes.
  * Merge requests that do not add a feature or fix a bug.
  * Merge requests that only add comments.
  * Merge requests that only fix typos in code comments.
  * Merge requests that only consist of small code refactors.
  
  There is a monthly User Feedback & Suggested Changes merge for unacceptable merge requests. The changes are based on feedback in the [March 2019 - User Feedback & Suggested Changes](https://gitlab.com/sm64hacks/webapp/issues/1) Issue and implemented by a team member. The more details you provide (codeblocks are great) in your comments, the better the merge will be at the end of the month.