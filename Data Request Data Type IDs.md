# Record Types

These are the IDs of Record Types which require a formal request processed before the data may be accessed.

1) User
2) Report

## One-liners

These are a few "one-liners" site security engineers can use to fetch information from data requests.

**IP Address of a Report**
```
x = 1
Report.find_by_id(DataRequest.find_by_id(x).record_id).ipaddress
```