# Configuring Nginx

You may choose to run the application behind nginx or a similar load balancer. Ruby on Rails has a CSRF Filter which protects the application from malicious requests. If not configured properly load balancers can confuse the filter. This documentation contains information on making it work with nginx.

## Reverse Proxy to the Application
When running behind nginx, the rails application can get confused about a request and block it as a security precation. The following configuration works well.
```
# https://stackoverflow.com/a/51111144
upstream myapp {
  server              unix:///path/to/puma.sock;
}
...
location / {
  proxy_pass        http://myapp;
  proxy_set_header  Host $host;
  proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header  X-Forwarded-Proto $scheme;
  proxy_set_header  X-Forwarded-Ssl on; # Optional
  proxy_set_header  X-Forwarded-Port $server_port;
  proxy_set_header  X-Forwarded-Host $host;
}
```