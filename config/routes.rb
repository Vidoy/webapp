Rails.application.routes.draw do
  # Page Routes
  get '/' => 'pages#index'
  get '/help' => 'pages#help'
  get '/test' => 'pages#test'
  get '/error' => 'pages#error'
  get '/requested/:id' => 'pages#requested'

  # Hack Routes
  resources :hacks
  get '/all' => 'hacks#all'
  get '/random' => 'hacks#random'

  get '/hack/:id' => 'hacks#view'
  get '/hack/edit/:id' => 'hacks#edit'
  post '/hack/save/:id' => 'hacks#save'
  get '/hack/delete/:id' => 'hacks#delete'
  post '/hack/confirmdelete/:id' => 'hacks#confirm_delete'

  get '/hack/report/:id' => 'hacks#add_report'
  post '/hack/report/:id' => 'hacks#create_report'

  get '/hack/version/add/:id' => 'hacks#add_version'
  post '/hack/version/create/:id' => 'hacks#create_version'
  get '/hack/version/edit/:id' => 'hacks#edit_version'
  post '/hack/version/save/:id' => 'hacks#save_version'
  get '/hack/version/delete/:id' => 'hacks#delete_version'
  post '/hack/version/confirmdelete/:id' => 'hacks#confirm_delete_version'

  # Search Routes
  get '/search' => 'search#index'

  # Admin Routes
  get '/admin' => 'admin#index'
  get '/admin/user/list' => 'admin#list_users'
  get '/admin/user/password/:id' => 'admin#change_user_password'
  post '/admin/user/password/:id' => 'admin#save_change_user_password'
  get '/admin/user/delete/:id' => 'admin#delete_user'
  post '/admin/user/delete/:id' => 'admin#confirm_delete_user'
  get '/admin/user/grant/:id' => 'admin#grant_admin'
  post '/admin/user/grant/:id' => 'admin#confirm_grant_admin'
  get '/admin/user/revoke/:id' => 'admin#revoke_admin'
  post '/admin/user/revoke/:id' => 'admin#confirm_revoke_admin'
  get '/admin/reports/list' => 'admin#list_user_reports'
  get '/admin/reports/delete/:id' => 'admin#delete_user_report'
  post '/admin/reports/delete/:id' => 'admin#confirm_delete_user_report'
  get '/admin/reports/ban/:id' => 'admin#ban_reporter'
  post '/admin/reports/ban/:id' => 'admin#confirm_ban_reporter'
  get '/admin/reports/:id/ipaddress' => 'admin#request_report_ip_address'
  post '/admin/reports/:id/ipaddress' => 'admin#submit_request_report_ip_address'
  get '/admin/hack/add' => 'hacks#add'
  post '/admin/hack/add' => 'hacks#create'
  get '/admin/test' => 'admin#test'

  # Authentication Routes
  resources :users
  get '/signup'  => 'users#new'
  get '/login' => 'sessions#new'
	post '/login' => 'sessions#create'
  delete '/logout' => 'sessions#destroy'

end
